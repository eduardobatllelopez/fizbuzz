package com.itau.fizzbuzz;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
//		if (i % 3 == 0) {
//			return "fizz";
//		}
//		return null;
		if ((i % 3 ==0) && (i % 5 ==0))  {
			return "fizzbuzz";
		}
		
		if (i % 5 == 0) {
			return "buzz";
		}
		
		if ( i % 3 == 0) {
			return "fizz";
			
		}

		return Integer.toString(i);
				
	}
	
	public static String nova_funcao(int i) {
		String res = "";
		for(int j = i; j>0; j--) {
			res += fizzBuzz(j) + " ";
		}
		return res;
	}

	
}
